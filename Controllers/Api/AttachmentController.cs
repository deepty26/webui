﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebUI.Interface;
using WebUI.Models;

namespace WebUI.Controllers.Api
{
    [Route("api/v1/[controller]")]
    [ApiController]

    public class AttachmentController : ControllerBase
    {
        private IAttachmentRepository _repo;
        public AttachmentController(IAttachmentRepository repo)
        {
            _repo = repo;
        }

        //Post Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create1(Attachment attachment)
        {
            if (ModelState.IsValid)
            {
               
                await _repo.CreateSync(attachment);
                return CreatedAtAction(nameof(Index), new { id = attachment.AttachmentId }, attachment);
            }
            return BadRequest(ModelState);
        }

        
    }
}
