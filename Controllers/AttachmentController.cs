﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebUI.Interface;
using WebUI.Models;
using WebUI.ViewModel;

namespace WebUI.Controllers
{
    //[Route("api/v1/[controller]")]
    //[ApiController]

    public class AttachmentController : Controller //: ControllerBase 
    {
        //Injection interface to the controller 
        private IAttachmentRepository _repo;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public AttachmentController (IAttachmentRepository repo, IWebHostEnvironment webHostEnvironment)
        {
            _repo = repo;
            _webHostEnvironment = webHostEnvironment;
        }

        //[HttpGet]
        //[Route("GetAll")]
        public async Task<IActionResult> Index()//[FromQuery] Models.PurchaseRequisition.PurchaseRequisition pr
        {
            var pa = await _repo.GetAllSync();
            //return Ok(pa);
            return View(pa);
        }

        //UI Create
        public async Task<IActionResult> Create()
        {
            return View();
        }

        public async Task<IActionResult> Add()
        {
            return View();
        }

        //Post Create
        [HttpPost]
        //to prevent from malicious command or script from trusted browser
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AttachmentViewModel model)
        {            
            if (ModelState.IsValid)
            {
                Attachment attachment = new Attachment
                {                    
                    FileName = UploadedFile(model),
                    Path = Path.Combine(_webHostEnvironment.WebRootPath, "images")
            };

                await _repo.CreateSync(attachment);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        //storing attachment in the table 
        private string UploadedFile(AttachmentViewModel model)
        {
            string fileName = null;           

            if (model.FileName != null)
            {
                //Images folder in the project where storing all the files 
                //to display the path 
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                //if need to store with uniquename
                //uniqueFileName = Guid.NewGuid().ToString() + "_" + model.FileName.FileName;
                fileName = model.FileName.FileName;
                string filePath = Path.Combine(uploadsFolder, fileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    model.FileName.CopyTo(fileStream);
                }
            }
            return fileName;
        }

    }
}
