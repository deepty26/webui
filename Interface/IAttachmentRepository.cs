﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebUI.Models;

namespace WebUI.Interface
{
    public interface IAttachmentRepository
    {
        Task<Attachment> CreateSync(Attachment attachment);
        Task<IEnumerable<Attachment>> GetAllSync();
    }
}
