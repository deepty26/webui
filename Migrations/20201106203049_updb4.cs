﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebUI.Migrations
{
    public partial class updb4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AttachmentName",
                table: "Attachments",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Path",
                table: "Attachments",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AttachmentName",
                table: "Attachments");

            migrationBuilder.DropColumn(
                name: "Path",
                table: "Attachments");
        }
    }
}
