﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.Models
{
    public class Attachment
    {
        [Key]
        public Guid AttachmentId { get; set; }        
        [Required]
        public string Path { get; set; }
        [Required]
        public string FileName { get; set; }        
        public DateTime EntryDate { get; set; } = DateTime.Now;
       
    }
}
