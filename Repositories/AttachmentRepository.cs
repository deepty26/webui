﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebUI.Interface;
using WebUI.Models;

namespace WebUI.Repositories
{
    public class AttachmentRepository : IAttachmentRepository
    {
        private readonly ApplicationDbContext _ctx;

        public AttachmentRepository (ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }
        public async Task<Attachment> CreateSync(Attachment attachment)
        {
            await _ctx.Attachments.AddAsync(attachment);
            await _ctx.SaveChangesAsync();
            return attachment;

        }

        public async Task<IEnumerable<Attachment>> GetAllSync()
        {
            if (_ctx != null)
            {
                return await _ctx.Attachments.ToListAsync();
            }

            return null;
        }

    }
}
