﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.Utilities
{
    public static class Constants
    {
        public const string EntryDate = "Date";
        public const string UploadConfirmationMessage = "Are you sure you want to upload?";
        public const string FileName = "File Name";
        public const string AttachmentName = "Attachment Name";
        public const string Path = "File Location";
        public const string View = "View";
        public const string Upload = "Upload";
        public const string Submit = "Submit";

        public const string AttachmentNameErrorMessage = "Enter Name of the File";
        public const string FileNameErrorMessage = "Please select a file";

        public const string NotFound = "No Records Found";
        public const string UploadFiles = "Upload Files";
    }
}
