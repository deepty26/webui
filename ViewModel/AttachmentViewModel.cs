﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModel
{
    public class AttachmentViewModel
    {        
        public DateTime EntryDate { get; set; } = DateTime.Now;

        [Required(ErrorMessage = Utilities.Constants.FileNameErrorMessage)]
        [Display(Name = Utilities.Constants.FileName)]
        public IFormFile FileName { get; set; } 
       
        public string Path { get; set; }
    }
}
