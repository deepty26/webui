﻿$SourcePath = "\\10.2.2.91\C$\DeploymentPackage\OAS-Enterprise\*"
$DestPath = "\\10.2.2.60\E$\www-data\backups"
$IsAccess = (Get-Acl $DestPath).Access | ?{$_.IdentityReference -match $User} | Select IdentityReference,FileSystemRights
$SiteName = "oas-testback.ngap.com"

if($IsAccess){

    New-Item -ItemType Directory -Path $DestPath\$SiteName
    copy-Item -path $SourcePath  -Destination $DestPath\$SiteName -Recurse -Force
    Write-Output "Files have been copied!"

    Remove-Item $DestPath\$SiteName\node_modules -Recurse -Force 
    Write-Output " Node modules is been deleted"

    New-Item -ItemType Directory -Path $DestPath\$SiteName\node_modules
    Write-Output "node_modules folder is created!"

    Enter-PSSession -ComputerName "\\10.2.2.60\E$\www-data\backups\oas-testback.ngap.com\node_modules"
    npm install
    ./migrate.bat
    Write-Output "npm install is done!"
}
else
{
    Write-Output "Please contact to Administrator !!!"
}

